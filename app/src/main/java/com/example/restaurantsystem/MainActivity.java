package com.example.restaurantsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import org.json.JSONObject;
import org.json.JSONArray;
import java.util.ArrayList;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    Button btn01, btn02;
    Button submit;
    Button btngt;
    EditText et01;
    TextView text1;
    ImageView img1;
    String url = "http://172.20.10.2:5000";
    int total = 0;
    boolean terminate = false;

    //JSONArray list_of_data = null;

    JSONArray list_of_data;

    int map_cnt = 0;
    Map<String,Integer> map = new HashMap<String,Integer>();

    private Handler mainHandler = new Handler();
    //private volatile boolean stopThread = false;
    private Context context = MainActivity.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        terminate = false;

        /*btn01 = findViewById(R.id.btn01);
        btn02 = findViewById(R.id.btn02);
        submit = findViewById(R.id.submit);

        et01 = findViewById(R.id.et01);

        btngt = findViewById(R.id.btngt);

        text1 = findViewById(R.id.text1);

        img1 = findViewById(R.id.img1);

        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = et01.getText().toString();
                int i = Integer.parseInt(value);
                if (i - 1 < 0) {
                    System.out.println("FUCK");
                } else {
                    et01.setText(String.valueOf(i - 1));
                }
//                Log.d("MAIN", "test1");//tag為msg(輸出)的名字
//                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
//                startActivity(intent);
//                //finish();
//                Log.d("istest","hdfhk");
            }
        });
        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = et01.getText().toString();
                int i = Integer.parseInt(value);
                if (i + 1 > 99) {
                    System.out.println("FUCK");
                } else {
                    et01.setText(String.valueOf(i + 1));
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = et01.getText().toString();
                int i = Integer.parseInt(value);
                System.out.println(i);
                //HttpClient client = new DefaultHttpClient();
            }
        });



        btngt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Glide.with(context).load("http://10.0.2.2:5000/upload/salad.jpg").into(img1);
            }
        });*/

        /*get_data_thread getdatathread = new get_data_thread();
        new Thread(getdatathread).start();*/

        //


        LinearLayout menu = findViewById(R.id.menu);

        EditText table = findViewById(R.id.table);

        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,50);
        /*EditText table = new TextView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150,175);
        params.setMargins(10,10,10,10);
        table.setLayoutParams(params);
        table.setText("0");
        table.setInputType(InputType.TYPE_CLASS_NUMBER);
        menu.addView(table);*/


        TextView total_price = findViewById(R.id.total_price);
        /*TextView total_price = new TextView(context);
        total_price.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 50));
        total_price.setText("The total is : $"+String.valueOf(total));
        menu.addView(total_price);*/

        Button submit_btn = findViewById(R.id.submit_btn);
        /*Button submit_btn = new Button(context);
        submit_btn.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 175));
        //submit_btn.setId(map_cnt);
        submit_btn.setText("Place Order");
        //map.put("submit_btn",map_cnt);
        //map_cnt++;
        menu.addView(submit_btn);*/
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ){
                if(table.getText().toString()=="0" || total <= 0){Log.d("msg","no table");}
                else{
                    terminate = true;
                    //Map<String,String> data;
                    HashMap<String, String> data = new HashMap<>();
                    data.put(table.getText().toString(),"table");
                    String[] table_name = new String[1];
                    for(int i = 0 ; i < list_of_data.length() ; i++){
                        try {
                            table_name[0] = list_of_data.getJSONArray(i).getString(0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //ArrayList data1 = new ArrayList();
                        try {
                            //data1.add(list_of_data.getJSONArray(i).getJSONArray(1));
                            int jcnt = list_of_data.getJSONArray(i).getJSONArray(1).length();
                            //HashMap<String, String> data2 = new HashMap<>();
                            for(int j = 0 ; j < jcnt ; j++){
                                //ArrayList data2 = new ArrayList();
                                String[] dish_name = new String [1];
                                //String[] dish_classify = new String [1];
                                //Log.d("content", list_of_data.toString());
                                dish_name[0] = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(1);
                                //dish_classify[0] = list_of_data.getJSONArray(i).getJSONArray(0).getString(0);
                                //data2.add(dish_name[0]);
                                TextView amount = findViewById(map.get(dish_name[0]+"_edit"));
                                //data2.put(dish_name[0],amount.getText().toString());
                                String edit = amount.getText().toString();

                                if(edit!="0"){data.put(table_name[0]+"!"+dish_name[0],edit);
                                    Log.d(table_name[0], dish_name[0]);
                                }
                                Log.d("all",data.toString());


                                //data2.add(amount.getText().toString());
                                //data1.add(data2);
                                //assert data != null;

                            }
                            //data.putAll(data2);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //data.add(data2);

                    }


                    OkHttpClient client = new OkHttpClient().newBuilder().build();
                    //Log.d("size of list_of_data",Integer.toString(list_of_data.length()));

                    FormBody.Builder builder = new FormBody.Builder().add("table",table.getText().toString());
                    Iterator it = data.entrySet().iterator();
                    while(it.hasNext()){
                        Map.Entry pair = (Map.Entry)it.next();
                        builder.add(pair.getKey().toString(),pair.getValue().toString());
                    }
                    RequestBody formbody = builder.build();

                    Request request = new Request.Builder().url(url+"/orders_return").post(formbody).build();

                    Call call = client.newCall(request);

                    call.enqueue(new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            Intent switchActivityIntent = new Intent(context, MainActivity_complete.class);
                            switchActivityIntent.putExtra("total", Integer.toString(total));
                            startActivity(switchActivityIntent);
                            finish();
                            //Thread.currentThread().interrupt();
                        }

                        @Override
                        public void onFailure(Call call, IOException e) {
                            Log.d("Error", e.toString());
                            Intent switchActivityIntent = new Intent(context, MainActivity_connection_error.class);
                            startActivity(switchActivityIntent);
                            finish();
                            //Thread.currentThread().interrupt();
                        }
                    });
                }
            }
        });



        OkHttpClient client = new OkHttpClient().newBuilder().build();

        FormBody formbody = new FormBody.Builder().build();

        Request request = new Request.Builder().url(url+"/orders").post(formbody).build();

        Call call = client.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, @NotNull Response response) throws IOException {

                String result = response.body().string();
                try {
                    list_of_data = new JSONArray(result);
                    //Log.d("list2", list_of_data.toString());

                    LinearLayout parent = findViewById(R.id.menu);
                    for(int i = 0 ; i < list_of_data.length() ; i++){
                        TextView dish_type = new TextView(context);
                        try {
                            dish_type.setText(list_of_data.getJSONArray(i).getString(0));
                        } catch (JSONException e) {
                            e.printStackTrace();

                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(getIntent());
                            overridePendingTransition(0, 0);
                        }
                        LinearLayout dish_type_layer = new LinearLayout(context);
                        dish_type_layer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        dish_type_layer.setOrientation(LinearLayout.VERTICAL);

                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                parent.addView(dish_type);
                                parent.addView(dish_type_layer);
                            }
                        });


                        int jcnt = 0;
                        try {
                            jcnt = list_of_data.getJSONArray(i).getJSONArray(1).length();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for(int j = 0 ; j < jcnt ; j++){

                            String[] dish_name = new String[1];
                            String[] dish_price = new String[1];
                            String[] dish_amount = new String[1];
                            String[] dish_url = new String[1];


                            try {
                                dish_name[0] = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                dish_price[0] = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(2);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                dish_amount[0] = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(3);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                dish_url[0] = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(4);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            LinearLayout dish_layer = new LinearLayout(context);
                            dish_layer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            dish_layer.setOrientation(LinearLayout.HORIZONTAL);
                            //dish_type_layer.addView(dish_layer);

                            ImageView food_picture = new ImageView(context);
                            food_picture.setLayoutParams(new LinearLayout.LayoutParams(250,250));
                            //dish_layer.addView(food_picture);

                            mainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dish_type_layer.addView(dish_layer);
                                    dish_layer.addView(food_picture);
                                    Glide.with(context).load(dish_url[0]).into(food_picture);
                                }
                            });



                            LinearLayout dish_other_layer = new LinearLayout(context);
                            dish_other_layer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            dish_other_layer.setOrientation(LinearLayout.VERTICAL);
                            //dish_layer.addView(dish_other_layer);

                            LinearLayout dish_detail_layer = new LinearLayout(context);
                            dish_detail_layer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            dish_detail_layer.setOrientation(LinearLayout.VERTICAL);
                            //dish_other_layer.addView(dish_detail_layer);

                            LinearLayout dish_ordering_layer = new LinearLayout(context);
                            dish_ordering_layer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            dish_ordering_layer.setOrientation(LinearLayout.HORIZONTAL);
                            //dish_other_layer.addView(dish_ordering_layer);

                            TextView Name = new TextView(context);
                            TextView Price = new TextView(context);
                            TextView Amount = new TextView(context);
                            Name.setText("Name : "+dish_name[0]);
                            Price.setText("Price : "+dish_price[0]);
                            Amount.setText("Remain Amount : "+dish_amount[0]);
                            Price.setId(map_cnt);
                            map.put(dish_name[0]+"_price",map_cnt);
                            map_cnt++;
                            Amount.setId(map_cnt);
                            map.put(dish_name[0]+"_amount",map_cnt);
                            map_cnt++;



                            mainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dish_layer.addView(dish_other_layer);
                                    dish_other_layer.addView(dish_detail_layer);
                                    dish_other_layer.addView(dish_ordering_layer);
                                    dish_detail_layer.addView(Name);
                                    dish_detail_layer.addView(Price);
                                    dish_detail_layer.addView(Amount);
                                }
                            });




                            Button btn_plus = new Button(context);
                            btn_plus.setLayoutParams(new LinearLayout.LayoutParams(250, 175));
                            btn_plus.setId(map_cnt);
                            btn_plus.setText(" + ");
                            map.put(dish_name[0]+"_plus",map_cnt);
                            map_cnt++;

                            Button btn_minus = new Button(context);
                            btn_minus.setLayoutParams(new LinearLayout.LayoutParams(250, 175));
                            btn_minus.setId(map_cnt);
                            btn_minus.setText(" - ");
                            map.put(dish_name[0]+"_minus",map_cnt);
                            map_cnt++;

                            /*EditText edit_price = new EditText(context);
                            //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,50);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150,175);
                            params.setMargins(10,10,10,10);
                            edit_price.setLayoutParams(params);
                            edit_price.setText("0");
                            edit_price.setId(map_cnt);
                            edit_price.setInputType(InputType.TYPE_CLASS_NUMBER);
                            map.put(dish_name[0]+"_edit",map_cnt);
                            map_cnt++;*/

                            TextView edit_price = new TextView(context);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150,175);
                            edit_price.setLayoutParams(params);
                            edit_price.setText("0");
                            edit_price.setId(map_cnt);
                            map.put(dish_name[0]+"_edit",map_cnt);
                            map_cnt++;


                            mainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dish_ordering_layer.addView(btn_plus);
                                    dish_ordering_layer.addView(edit_price);
                                    dish_ordering_layer.addView(btn_minus);

                                }
                            });


                            btn_plus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick( View v ){
                                    String value = edit_price.getText().toString();
                                    int i = Integer.parseInt(value);

                                    //TextView d_name = findViewById(map.get(dish_name[0]+"_amount"));
                                    String part_Amount = Amount.getText().toString();
                                    //Log.d("chk", part_Amount);
                                    int part_Amount_int = Integer.parseInt(part_Amount.substring(16,part_Amount.length()));

                                    if ( i+1 <= part_Amount_int) {
                                        edit_price.setText(String.valueOf(i + 1));
                                        total += Integer.parseInt(dish_price[0]);
                                        total_price.setText("The total is : $"+String.valueOf(total));
                                    }

                                }
                            });
                            btn_minus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick( View v ){
                                    String value = edit_price.getText().toString();
                                    int i = Integer.parseInt(value);
                                    if (i - 1 >= 0) {
                                        edit_price.setText(String.valueOf(i - 1));
                                        total -= Integer.parseInt(dish_price[0]);
                                        total_price.setText("The total is : $"+String.valueOf(total));
                                        //System.out.println("FUCK");
                                    }

                                }
                            });


                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                // 連線失敗
                Log.d("Error", e.toString());
                Intent switchActivityIntent = new Intent(context, MainActivity_connection_error.class);
                startActivity(switchActivityIntent);
                finish();
            }
        });
        //







        /*int resID1 = getResources().getIdentifier("_plus","id", getPackageName());
        Button btn_add = findViewById(resID1);


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v ){
                int resID1 = getResources().getIdentifier("_plus","id", getPackageName());
                Button btn_add = findViewById(resID1);
            }
        });*/


    Thread get_data_thread = new Thread(get_data);
    get_data_thread.start();


    }







    Runnable get_data = new Runnable() {

        @Override
        public void run() {
            while(true) {
                if(terminate == true){break;}
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                OkHttpClient client = new OkHttpClient().newBuilder().build();

                FormBody formbody = new FormBody.Builder().build();

                Request request = new Request.Builder().url(url+"/orders").post(formbody).build();

                Call call = client.newCall(request);

                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, @NotNull Response response) throws IOException {
                        String result = response.body().string();
                        try {
                            JSONArray list_of_data = new JSONArray(result);
                            for (int i = 0; i < list_of_data.length(); i++) {
                                for (int j = 0; j < list_of_data.getJSONArray(i).getJSONArray(1).length(); j++) {
                                    String price = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(6);
                                    String price_of = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(2);
                                    String amount = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(7);
                                    String amount_of = list_of_data.getJSONArray(i).getJSONArray(1).getJSONArray(j).getString(3);


                                    mainHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            TextView dish_price = findViewById(map.get(price));
                                            TextView dish_amount = findViewById(map.get(amount));
                                            dish_price.setText("Price : "+price_of);
                                            dish_amount.setText("Remain Amount : "+amount_of);
                                        }
                                    });

                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onFailure(Call call, IOException e) {
                        // 連線失敗
                        Log.d("Error", e.toString());
                        Intent switchActivityIntent = new Intent(context, MainActivity_connection_error.class);
                        startActivity(switchActivityIntent);
                        finish();
                    }
                });
            }
        }



    };



    //
}
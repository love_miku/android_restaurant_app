package com.example.restaurantsystem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity_connection_error extends AppCompatActivity {
    private Context context = MainActivity_connection_error.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_connection_error);

        Button button = findViewById(R.id.last_page);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent switchActivityIntent = new Intent(context, MainActivity.class);
                startActivity(switchActivityIntent);
                finish();
            }
        });
    }





}

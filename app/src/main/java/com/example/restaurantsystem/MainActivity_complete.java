package com.example.restaurantsystem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity_complete extends AppCompatActivity {


    private Context context = MainActivity_complete.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_comlpete);

        Intent intent = getIntent();
        String message = intent.getStringExtra("total");
        int total = 0;
        if(message == null){
            Log.d("error", "length is null");
        }
        else {
            total = Integer.parseInt(message);
        }

        TextView textview = findViewById(R.id.note);
        textview.setText("Ordered , The total is : $"+total);

        Button button = findViewById(R.id.last_page);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent switchActivityIntent = new Intent(context, MainActivity.class);
                startActivity(switchActivityIntent);
                finish();
            }
        });

    }
}
